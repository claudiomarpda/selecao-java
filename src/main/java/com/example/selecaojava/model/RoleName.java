package com.example.selecaojava.model;

public interface RoleName {

    String ROLE_ADMIN = "ROLE_Administrador";
    String ROLE_USER = "ROLE_Usuário";

    String ADMIN = "Administrador";
    String USER = "Usuário";

}
